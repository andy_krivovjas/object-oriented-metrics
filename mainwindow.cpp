#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "metrics.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>

Metrics metrics;
QString code = "";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    metrics = Metrics(code);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_openfile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "",tr("C++ Files (*.cpp *.h)"));
        if (fileName != "")
        {
                QFile file(fileName);
                if (!file.open(QIODevice::ReadOnly)) {
                    QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
                    return;
                }
                QTextStream in(&file);
                ui->plainTextEdit->setText(in.readAll());
                file.close();
        }
        code = ui->plainTextEdit->toPlainText();
        MainWindow::on_Metrics_clicked();
}

void MainWindow::on_Metrics_clicked()
{
    metrics.setCodeSource(code);

    ui->label_SrKSMet->setText(QString::number(metrics.getMethodsLinesAv()));
    ui->label_SrKSMod->setText(QString::number(metrics.getModulesLinesAv()));
    ui->label_SrKSCl->setText(QString::number(metrics.getClassLinesAv()));
    ui->label_WMC->setText(metrics.getWMC());
    ui->label_DIT->setText(metrics.getDIT());
    ui->label_NOC->setText(metrics.getNOC());
    ui->label_RFC->setText(QString::number(metrics.getRFC()));
    ui->label_LCOM->setText(metrics.getLCOM());
    ui->label_NORM->setText(QString::number(metrics.getNORM()));
    ui->Kol_Met->setText(QString::number(metrics.getMethodsLinesCount()));
    ui->Kol_Mod->setText(QString::number(metrics.getModulesLinesCount()));
    ui->Kol_Class->setText(QString::number(metrics.getClassLinesCount()));
    ui->Include->setText(QString::number(metrics.getIncludedLinesCount()));
}
