#ifndef METRICS_H
#define METRICS_H
#include <QString>
#include <QStringList>

class Metrics
{

private:
    QString codeEntity;

    int methodsLines = 0;
    float methodsLinesAv = 0;
    int modulesLines = 0;
    float modulesLinesAv = 0;
    int includeLines = 0;
    int classLines = 0;
    float classLinesAv = 0;
    int externalMethods = 0;
    int classResponses = 0;
    QString weightedMethods = "";
    QString lackOfCohesion = "";
    QString numberOfChildren = "";
    QString depthOfInheritance = "";

public:
    Metrics();
    Metrics(QString code);
    void setCodeSource(QString code);

    int getMethodsLinesCount();
    float getMethodsLinesAv();
    int getModulesLinesCount();
    float getModulesLinesAv();
    int getIncludedLinesCount();
    int getClassLinesCount();
    float getClassLinesAv();
    int getNORM();
    int getRFC();
    QString getWMC();
    QString getLCOM();
    QString getNOC();
    QString getDIT();
};

#endif // METRICS_H
