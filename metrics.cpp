#include "metrics.h"

Metrics::Metrics()
{

}

Metrics::Metrics(QString code)
{
    codeEntity = code;
}

void Metrics::setCodeSource(QString code)
{
    codeEntity = code;
}

int Metrics::getMethodsLinesCount()
{
    if(!methodsLines) {
        QStringList lst = codeEntity.split(QRegExp("(void|int|string|float|double|byte)[A-Za-z0-9]*"),  QString::SkipEmptyParts);
        methodsLines = lst.count() - 1;
    }
    return methodsLines;
}

float Metrics::getMethodsLinesAv()
{
    if(!methodsLinesAv) {
        QStringList lst = codeEntity.split(QRegExp("(void|int|string|float|double|byte)[A-Za-z0-9]*"),  QString::SkipEmptyParts);
        int s = 0;
        for(int i = lst.count()-1; i >= 0; --i)
        {
            const QString& item = lst[i];
            s=s+item.count("\n");
        }
        methodsLinesAv = (float) s / (this->getMethodsLinesCount() + 1);
    }
    return methodsLinesAv;

}

int Metrics::getModulesLinesCount()
{
    if(!modulesLines) {
        QStringList lst = codeEntity.split(QRegExp("//module"),  QString::SkipEmptyParts);
        modulesLines = lst.count()-1;
    }
    return modulesLines;
}

float Metrics::getModulesLinesAv()
{
    if(!modulesLinesAv) {
        QStringList lst = codeEntity.split(QRegExp("//module"),  QString::SkipEmptyParts);
        int s = 0;
        for(int i = lst.count()-1; i >= 0; --i)
        {
            const QString& item = lst[i];
            s=s+item.count("\n")-1;
        }
        modulesLinesAv = (float) s / (this->getModulesLinesCount() + 1);
    }
    return modulesLinesAv;
}

int Metrics::getIncludedLinesCount()
{
    if(!includeLines) {
        QStringList lst = codeEntity.split(QRegExp("#include"),  QString::SkipEmptyParts);
        includeLines = lst.count() - 1;
    }
    return includeLines;
}

int Metrics::getClassLinesCount()
{
    if(!classLines) {
        QStringList lst = codeEntity.split(QRegExp("class "),  QString::SkipEmptyParts);
        classLines = lst.count() - 1;
    }
    return classLines;
}

float Metrics::getClassLinesAv()
{
    if(!classLinesAv) {
        QStringList lst = codeEntity.split(QRegExp("class "),  QString::SkipEmptyParts);
        int s = 0;
        for(int i = lst.count()-1; i >= 0; --i)
        {
            const QString& item = lst[i];
            s=s+item.count("\n")-1;
        }
        classLinesAv = (float) s / (this->getClassLinesCount() + 1);
    }
    return classLinesAv;
}

int Metrics::getNORM()
{
    if(!externalMethods) {
        QStringList lst = codeEntity.split(QRegExp("class "),  QString::SkipEmptyParts);
        int s = 0;
        for(int i = lst.count()-1; i >= 0; --i)
        {
            const QString& item = lst[i];
            QStringList list = item.split("){",  QString::SkipEmptyParts);
            s=s+item.count(") ")-item.count("this.")-item.count(" ) ");
        }
        externalMethods = s;
    }
    return externalMethods;
}

int Metrics::getRFC()
{
    if(!classResponses) {
        QStringList lst = codeEntity.split(QRegExp("class "),  QString::SkipEmptyParts);
        int s = 0;
        for(int i = lst.count()-1; i >= 0; --i)
        {
            const QString& item = lst[i];
            QStringList list = item.split("){",  QString::SkipEmptyParts);
            s=s+item.count(") ")-item.count(" ) ");
        }
        classResponses = s;
    }
    return classResponses;
}

QString Metrics::getWMC()
{
    if(true) {
        weightedMethods = "";

        QStringList lst = codeEntity.split(QRegExp("class "),  QString::SkipEmptyParts);
        int s = 0;
        for(int i = lst.count()-1; i >= 0; --i)
        {
            const QString& item = lst[i];
            QStringList list = item.split("){",  QString::SkipEmptyParts);
            s=0;
            for(int j = list.count()-1; j >= 0; --j)
            {
                const QString& ite = list[j];
                if ((ite.count("{")>=ite.count("}"))&&(ite.count("}")!=0))
                    s=s+1;
            }
            weightedMethods += " " + QString::number(s);
        }
    }
    return weightedMethods;
}

QString Metrics::getLCOM()
{
    if(true) {
        lackOfCohesion = "";

        int P=0, Q=0;
        int m;
        QString coual="";
        QStringList lst = codeEntity.split(QRegExp("class "),  QString::SkipEmptyParts);
        int s = 0;
        for(int i = lst.count()-1; i >= 0; --i)
        {
            P=0;
            Q=0;
            const QString& item = lst[i];
            QStringList list = item.split("){",  QString::SkipEmptyParts);
            //QStringList::iterator t = list.begin();
            // s=s+item.count(") ")-item.count(" ) ");
            s=0;
            for(int j = list.count()-1; j >= 0; --j)
            {
                const QString& ite = list[j];
                //  if ((ite.count("{")>=ite.count("}"))&&(ite.count("}")!=0)) s=s+1;
                QStringList lt = ite.split(QRegExp("(void|int|string|float|double|byte)[A-Za-z0-9]*"),  QString::SkipEmptyParts);
                lt.removeDuplicates();
                m = lt.count();
                //  lt.removeAt(lt.count()-1);
                for(int g = lt.count()-1; g >= 1; --g)
                {
                    const QString& ir = lt[g];
                    QStringList l = ir.split(" ");
                    QString dd="+-*=:/%()";
                    for(int c = l.count()-1; c >= 1; --c)
                    {
                        const QString& r = l[c];
                        if(dd.count(r)==0)
                            coual=coual+r;
                        coual=coual+"++";
                    }
                }
            }
            QStringList vars = coual.split("++");
            for(int h = vars.count()-1; h >= 1; --h)
            {
                const QString& qw = vars[h];
                int y=0;
                for(int k = h; k >= 0; --k)
                {
                    const QString& qw1 = vars[k];
                    if (qw==qw1)
                        {P=P+1; break;}
                    else
                        y=y+1;
                    if ((k==0)&&(y==vars.count()-1-h))
                        Q=Q+1;
                }
            }
            float C = 0;
            float f1=1;
            float f2=1;
            for (int z=1; z<=m; z++)
                f1=f1*z;
            for (int z=1; z<=m-2; z++)
                f2=f2*z;
            C = sqrt(f1/2/f2);
            C = (P-Q)/C;
            lackOfCohesion += " " + QString::number(C);
        }
    }
    return lackOfCohesion;
}

QString Metrics::getNOC()
{
    if(true) {
        numberOfChildren = "";

        QStringList lst = codeEntity.split(QRegExp("class\s"),  QString::SkipEmptyParts);
        QStringList lis, Lclss, Rclss;
        for (int g = 0; g<lst.count(); g++)
        {
            const QString& item = lst[g].trimmed();
            QString St ="";
            for(int i=0; i<item.count(); i++)
                if (item[i]!='\n')
                    St=St+QString(item[i]);
                else
                    break;
            lis<<St;
        }
        for (int g = 0; g<lis.count(); g++)
        {
            const QString& item = lis[g].trimmed();
            QString St ="";
            for(int i=0; i<item.count(); i++)
                if (item[i]!=' ')
                    St=St+QString(item[i]);
                else break;
            Rclss<<St;
        }
        for (int g = 0; g<lis.count(); g++)
        {
            const QString& item = lis[g].trimmed();
            QString St ="";
            lst = item.split(QRegExp("public|protected|private "),  QString::SkipEmptyParts);
            for(int i=0; i<lst.count(); i++)
            {
                St = St+lst[i];
            }
            Lclss<<St;
        }
        for (int g = 0; g<Rclss.count(); g++)
        {
            Lclss[g].replace(0,Rclss[g].count(),"");
            Lclss[g] = Lclss[g].trimmed();
        }
        for (int g = 0; g<Rclss.count(); g++)
        {
            Rclss[g].remove(":");
            int s = 0;
            for (int i = 0; i<Rclss.count(); i++)
            {s=s+Lclss[i].count(Rclss[g]);}
            numberOfChildren += " " + Rclss[g] + " " + QString::number(s);
        }
    }
    return numberOfChildren;
}

QString Metrics::getDIT()
{
    if(true) {
        depthOfInheritance = "";

        QStringList lst = codeEntity.split(QRegExp("class\s"),  QString::SkipEmptyParts);
        QStringList lis, Lclss, Rclss;
        for (int g = 0; g<lst.count(); g++)
        {
            const QString& item = lst[g].trimmed();
            QString St ="";
            for(int i=0; i<item.count(); i++)
                if (item[i]!='\n')
                    St=St+QString(item[i]);
                else break;
            lis<<St;
        }
        for (int g = 0; g<lis.count(); g++)
        {
            const QString& item = lis[g].trimmed();
            QString St ="";
            for(int i=0; i<item.count(); i++)
                if (item[i]!=' ')
                    St=St+QString(item[i]);
                else break;
                   Rclss<<St;
        }
        for (int g = 0; g<lis.count(); g++)
        {
            const QString& item = lis[g].trimmed();
            QString St ="";
            lst = item.split(QRegExp("public|protected|private "),  QString::SkipEmptyParts);
            for(int i=0; i<lst.count(); i++)
                St = St+lst[i];
            Lclss<<St;
        }
        for (int g = 0; g<Rclss.count(); g++)
        {
            Lclss[g].replace(0,Rclss[g].count(),"");
            Lclss[g] = Lclss[g].trimmed();
        }
        for (int g = 0; g<Rclss.count(); g++)
        {
            Rclss[g].remove(":");
            Lclss[g].remove(",");
        }
        int** mass=new int*[Rclss.count()+1];
        for(int i=0; i<=Rclss.count();i++)
            mass[i]= new int [Rclss.count()+1];
        for (int g = 0; g<Rclss.count(); g++)
            for (int j = 0; j<Rclss.count(); j++)
            {
                mass[g][j]=0;
                if (g!=j)
                    {mass[g][j] = Lclss[g].count(Rclss[j]);}
            }
        int pst = 0;
        int prw = -1;
        int sum =0;
        int max = 0;
        depthOfInheritance = "";
        for (int i = 0; i<Rclss.count(); i++)
        {
            prw=i;
            pst=0;
            max = 0;
            sum = 0;
            while (pst<Rclss.count())
            {
                int k;
                if (mass[prw][pst]==1)
                {
                    if (prw == i)
                        k=pst;
                    mass[prw][pst]=0;
                    prw=pst;
                    pst=0;
                    sum=sum+1;
                }
                else pst=pst+1;
                if (max<sum)
                    max=sum;
                if ((pst==Rclss.count())&&(prw!=i))
                {
                    prw = i;
                    pst=0;
                    sum=0;
                }
                if ((pst==Rclss.count())&&(prw==i))
                    break;
            }
            for (int g = 0; g<Rclss.count(); g++)
                for (int j = 0; j<Rclss.count(); j++)
                {
                    mass[g][j]=0;
                    if (g!=j)
                        mass[g][j] = Lclss[g].count(Rclss[j]);
                }
            depthOfInheritance += " " + Rclss[i] + " " + QString::number(max);
        }
    }
    return depthOfInheritance;
}

